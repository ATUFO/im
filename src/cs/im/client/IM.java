package cs.im.client;

import cs.core.client.Client;
import cs.core.base.BaseMsgUtils;

import java.io.IOException;
import java.util.Scanner;

public class IM {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        Client client = new Client();

        client.setOnMessageCallback((key,str) -> {
            System.out.println(str);
        });

        client.setOnCloseCallback(key -> {
            System.exit(0);
        });

        Thread thread = new Thread(client);
        thread.start();
        while (true) {
            String str = scanner.next();
            BaseMsgUtils.sendMsg(client.getServerKey(), str);
        }

    }
}
