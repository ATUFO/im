package cs.im.server;

import cs.core.server.Server;
import cs.im.common.MsgUtils;
import cs.im.common.User;
import cs.im.common.Utils;

import java.io.IOException;

public class IMServerBuilder {
    /**
     * 构造的Server
     */
    public static Server build()  {

        Server server = null;
        try {
            server = new Server();
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert server != null;
        server.setOnMessageCallback((key, str) -> { //设置收到消息时的回调
            User curUser = Utils.getUserByKey(key);
            for (User user : Utils.getUserSet()) {
                if (user == curUser) continue;
                MsgUtils.sendMessage(user, String.format("%-8s: ", curUser.getUserName()) + str);
            }

        });

        server.setOnAcceptCallback((key) -> { //设置请求连接的回调
            User user = Utils.bindUser(key);
            MsgUtils.sendMessage(user, "你好，你本次使用的昵称是：" + user.getUserName() + "\n当前有 " + Utils.getOnlineUserNum() + " 人在线");
            for (User u : Utils.getUserSet()) {
                if (u == user) continue;
                MsgUtils.sendMessage(u, String.format("%s 上线了 ", user.getUserName()));
            }
        });

        server.setOnCloseCallback((key -> {
            String name = Utils.unBindUser(key);
            for (User user : Utils.getUserSet()) {
                MsgUtils.sendMessage(user, String.format("%s 离线了", name));
            }
        }));
        return server;
    }

}
