package cs.im.server;

import cs.im.common.User;
import cs.im.common.Utils;

import java.io.IOException;
import java.util.Scanner;

/**
 * 管理员操作界面
 */
public class AdminOps implements Runnable {
    Scanner scanner = new Scanner(System.in);

    public boolean isRunning = true;

    public void listAllUser(){
        for (User user : Utils.getUserSet()) {
            System.out.println(String.format("      >  %-4d %-5s",user.getId(),user.getUserName()));
        }
    }
    public void kickUser() throws IOException {
        System.out.println("请输入用户id");
        int id = scanner.nextInt();
        Utils.kickUserById(id);

    }

    @Override
    public void run() {
        while (true){
            System.out.println("1. 查看在线用户");
            System.out.println("2. 下线用户");
            int cmd = scanner.nextInt();
            switch (cmd){
                case 1:
                    listAllUser();
                    break;
                case 2:
                    try {
                        kickUser();
                    } catch (IOException e) {
                        isRunning = false;
                        scanner.close();
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}
