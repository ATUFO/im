package cs.im.server;

import cs.core.server.Server;

public class Daemon {

    static Thread adminOpsThread = new Thread(), imServerThread = new Thread();
    static AdminOps adminOps = new AdminOps();
    static Server server = IMServerBuilder.build();

    public static void main(String[] args) throws InterruptedException {
        while (true) {
            checkIMServerthread();
            checkAdminOpsThread();
            Thread.sleep(2000);
        }
    }

    /**
     * 检查UI情况
     * @return 正常返回true，否则返回false
     */
    public static boolean checkAdminOpsThread() {
        if(adminOpsThread.getState()!=Thread.State.RUNNABLE||!adminOps.isRunning){
            adminOps = new AdminOps();
            adminOpsThread.stop();
            adminOpsThread = new Thread(adminOps);
            adminOpsThread.start();
        }
        return true;
    }

    /**
     * 检查服务线程情况
     * @return 正常返回true,否则返回false
     */
    public static boolean checkIMServerthread() {
        if(imServerThread.getState()!=Thread.State.RUNNABLE||!server.isRunning){
            imServerThread.stop();
            server = IMServerBuilder.build();
            imServerThread = new Thread(server);
            imServerThread.start();
        }
        return true;
    }

}

