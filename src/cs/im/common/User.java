package cs.im.common;

public class User {
    private static int idCnt=0;
    private int id;
    private String userName;

    public User(String userName) {
        this.userName = userName;
        this.id = ++idCnt;
    }

    public int getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
