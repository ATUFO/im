package cs.im.common;

import cs.core.base.Config;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.util.*;

public class Utils {
    private static Set<String> usedNames=new HashSet<>(); // 用户名使用集
    private static HashMap<User, SelectionKey> userKeys = new HashMap<>();//用户key绑定
    private static HashMap<SelectionKey, User> keyUsers = new HashMap<>();//用户key绑定
    private static HashMap<Integer,User> idUsers = new HashMap<>();

    public static User bindUser(SelectionKey key){ //绑定用户
        String name = getRandomName();
        User user = new User(name);
        userKeys.put(user,key);
        keyUsers.put(key,user);
        idUsers.put(user.getId(),user);
        return user;
    }

    /**
     * 获取在线用户数
     * @return 用户数
     */
    public static  int getOnlineUserNum(){
        return userKeys.size();
    }

    /**
     * 根据用户id剔除用户
     * @param id
     * @throws IOException
     */
    public static void kickUserById(int id) throws IOException {
        User user = getUserById(id);
        SelectionKey key = getKeyByUser(user);
        MsgUtils.sendMessage(user,"你已被下线");
        key.channel().close();
        unBindUser(key);

    }

    /**
     * 解除绑定用户
     * @param key
     * @return
     */
    public static String unBindUser(SelectionKey key){
        User user = getUserByKey(key);
        String name = user.getUserName();
        usedNames.remove(name);
        userKeys.remove(user);
        keyUsers.remove(key);
        idUsers.remove(user.getId());
        return name;
    }

    public static User getUserById(int id){
        return idUsers.get(id);
    }

    public static User getUserByKey(SelectionKey key){
        return keyUsers.get(key);
    }

    public static Set<User> getUserSet(){
        return userKeys.keySet();
    }

    public static SelectionKey getKeyByUser(User user){
        return userKeys.get(user);
    }

    public static String getRandomName(){ //获取随机名字
        if(usedNames.size()== Config.names.length) {
            throw new RuntimeException("人数过多");
        }
        Random random = new Random();
        int idx = random.nextInt(Config.names.length);
        String name = Config.names[idx];
        while (usedNames.contains(name)){
            idx = random.nextInt(Config.names.length);
            name = Config.names[idx];
        }
        usedNames.add(name);
        return name;
    }

    public static void clear(){

    }

}
