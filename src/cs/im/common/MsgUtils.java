package cs.im.common;

import cs.core.base.BaseMsgUtils;

import java.io.IOException;
import java.nio.channels.SelectionKey;

public class MsgUtils {
    /**
     * 给指定用户发送消息
     * @param to 收信用户
     * @param con 内容
     * @throws IOException
     */
    public static void sendMessage(User to,String con) throws IOException {
        SelectionKey key = Utils.getKeyByUser(to);
        BaseMsgUtils.sendMsg(key,con);
    }
}
