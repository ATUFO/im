package cs.core.server;

import cs.core.base.Config;
import cs.core.base.EventHandler;
import cs.core.base.NioCore;
import cs.core.base.ifce.OnAccept;
import cs.core.base.ifce.OnClose;
import cs.core.base.ifce.OnMessage;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.channels.*;
import java.util.Iterator;
import java.util.Set;

public class Server implements Runnable{
    ServerSocketChannel serverSocketChannel;ServerSocket serverSocket;
    EventHandler handler=new ServerHandler();
    public boolean isRunning = true;
    public Server() throws IOException {
        serverSocketChannel= ServerSocketChannel.open();
    }

    public void start() throws IOException {
        Selector selector = NioCore.selector;
        serverSocketChannel.configureBlocking(false);
        serverSocket = serverSocketChannel.socket();
        serverSocket.setReuseAddress(true);
        serverSocket.bind(new InetSocketAddress(Config.PORT));


        // 循环处理
        while (true) {
            serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
            // 获取监听事件
            Set<SelectionKey> selectionKeys = selector.selectedKeys();
            Iterator<SelectionKey> iterator = selectionKeys.iterator();
            // 当注册事件到达时，方法返回，否则该方法会一直阻塞
            selector.select();
            while (iterator.hasNext()) {
                // 获取事件
                SelectionKey key = iterator.next();
                iterator.remove();
                handler.handler(key); //处理事件
            }

        }
    }


    @Override
    public void run() {
        try {
            start();
        } catch (IOException e) {
            try {
                System.out.println("close");
                serverSocketChannel.close();
                Thread.currentThread().stop();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
            isRunning=false;
            e.printStackTrace();
        }
    }

    public void setOnMessageCallback(OnMessage callback){
        handler.setOnMessage(callback);
    }

    public void setOnAcceptCallback(OnAccept onAccept){
        handler.setOnAccept(onAccept);
    }

    public void setOnCloseCallback(OnClose onClose){
        handler.setOnClose(onClose);
    }

}

