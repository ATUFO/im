package cs.core.server;

import cs.core.base.EventHandler;

import java.io.IOException;
import java.nio.channels.SelectionKey;

public class ServerHandler extends EventHandler {
    @Override
    public void handler(SelectionKey key) {
        try {
            if (key.isAcceptable()) {
                acceptHandler(key);
            }
            if (key.isReadable()) {
                readHandler(key);
            }
        }catch (Exception e){
            try {
                key.channel().close();
            } catch (IOException ioException) {

            }
            System.out.println(e.getMessage());
        }
    }
}
