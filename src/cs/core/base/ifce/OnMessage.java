package cs.core.base.ifce;

import java.io.IOException;
import java.nio.channels.SelectionKey;

@FunctionalInterface
public interface OnMessage {
    void act(SelectionKey key,String msg) throws IOException;
}
