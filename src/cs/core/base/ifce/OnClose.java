package cs.core.base.ifce;

import java.io.IOException;
import java.nio.channels.SelectionKey;

@FunctionalInterface
public interface OnClose {
    void act(SelectionKey key) throws IOException;
}
