package cs.core.base.ifce;

import java.io.IOException;
import java.nio.channels.SelectionKey;

@FunctionalInterface
public interface OnAccept {
    void act(SelectionKey key) throws IOException;
}
