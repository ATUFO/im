package cs.core.base;

import cs.core.base.ifce.OnAccept;
import cs.core.base.ifce.OnClose;
import cs.core.base.ifce.OnMessage;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

public  abstract class EventHandler {

    public OnMessage onMessage=((key, msg) -> {}); //空回调函数实现
    public OnAccept onAccept=(key -> {});
    public OnClose onClose=(key -> {});

    public abstract void handler(SelectionKey key);

    public void acceptHandler(SelectionKey key) throws IOException {
        SocketChannel socketChannel = ((ServerSocketChannel) key.channel()).accept();
        socketChannel.configureBlocking(false);
        SelectionKey readKey = socketChannel.register(NioCore.selector, SelectionKey.OP_READ, ByteBuffer.allocate(Config.BUFF_SIZE));
        onAccept.act(readKey);
    }

    public  void readHandler(SelectionKey key) throws IOException {
        SocketChannel socketChannel = (SocketChannel) key.channel();
        ByteBuffer byteBuffer = (ByteBuffer) key.attachment();
        String con = null;
        byteBuffer.clear();
        try {
            if (socketChannel.read(byteBuffer) > 0) {
                byteBuffer.flip();
                byte[] bytes = new byte[byteBuffer.limit()];
                byteBuffer.get(bytes,0,byteBuffer.limit());
                con = new String(bytes);
                byteBuffer.rewind();
            } else {
                key.channel().close(); // =0 时是服务端主动关闭
                onClose.act(key);
                return;
            }
        } catch (IOException e) {
            System.out.println(e);
            key.channel().close();
            onClose.act(key);
            return;
        }
        onMessage.act(key,con); //没有出错，调用回调函数
    }

    public void setOnMessage(OnMessage onMessage) {
        this.onMessage = onMessage;
    }

    public void setOnAccept(OnAccept onAccept) {
        this.onAccept=onAccept;
    }

    public void setOnClose(OnClose onClose) {
        this.onClose = onClose;
    }
}
