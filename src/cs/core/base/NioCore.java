package cs.core.base;

import java.io.IOException;
import java.nio.channels.Selector;

public class NioCore {
    static public Selector selector;
    static {
        try {
            selector = Selector.open();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
