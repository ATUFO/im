package cs.core.client;

import cs.core.base.EventHandler;

import java.io.IOException;
import java.nio.channels.SelectionKey;

public class ClientHandler extends EventHandler {
    @Override
    public void handler(SelectionKey key) {
        try {
            if (key.isAcceptable()) {
                acceptHandler(key);
            }
            if (key.isReadable()) {
               readHandler(key);
            }
        }catch (Exception e){
            try {
                key.channel().close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
            System.out.println(e.getMessage());
        }
    }
}
