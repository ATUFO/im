package cs.core.client;


import cs.core.base.EventHandler;
import cs.core.base.Config;
import cs.core.base.NioCore;
import cs.core.base.ifce.OnClose;
import cs.core.base.ifce.OnMessage;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

public class Client implements Runnable{

    SelectionKey serverKey;
    EventHandler handler = new ClientHandler();

    public void connect() throws IOException {
        SocketChannel sc =SocketChannel.open(new InetSocketAddress("localhost", Config.PORT));
        sc.configureBlocking(false);
        Selector selector = NioCore.selector;
        serverKey = sc.register(selector, SelectionKey.OP_READ,ByteBuffer.allocate(1024));

        while (true) {
            // 获取监听事件
            Set<SelectionKey> selectionKeys = selector.selectedKeys();
            Iterator<SelectionKey> iterator = selectionKeys.iterator();
            // 当注册事件到达时，方法返回，否则该方法会一直阻塞
            selector.select();
            while (iterator.hasNext()) {
                // 获取事件
                SelectionKey key = iterator.next();
                iterator.remove();
                handler.handler(key); //处理事件
            }

        }
    }

    public void start() throws IOException {
        connect();
    }

    @Override
    public void run() {
        try {
            start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public SelectionKey getServerKey() {
        return serverKey;
    }

    public void setOnMessageCallback(OnMessage callback){
        handler.setOnMessage(callback);
    }
    public void setOnCloseCallback(OnClose callback){
        handler.setOnClose(callback);
    }
}
